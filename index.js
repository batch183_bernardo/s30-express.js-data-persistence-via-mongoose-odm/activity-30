// Include express module
const express = require("express");
// Mongoose is a package allows creation of Schemas to model our data structure.
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List tthe port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to database by passing in your conenection string.
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String">, {userNewUrlParser: true, useUnifiedTopology: true});
*/

mongoose.connect("mongodb+srv://admin:admin@course-booking.ubhpmvq.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// If a connection error occured, it will be output in the console.
// console.error.bind(console) allows us to print the error in the browser console and in the terminal.
db.on("error", console.error.bind(console, "connection error"))

// If the connection is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."))

// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprints of our data.
const taskSchema = new mongoose.Schema({
	// Name of the task
	name : String, //name: String is a shorthand for {type: String}
	// Status task (Complete, Pending, Incomplete)
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server(JS code) to our database.

/*
	Syntax:
		const modelName = mongoose.model("collectionName", mongooseSchema);
*/
	// modelName must be in singular form and capitalize the first letter.
	// Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.
	const Task = mongoose.model("Task", taskSchema);

	const User = mongoose.model("User", userSchema);
//Middleware
app.use(express.json()); //Allow your app to read json data
app.use(express.urlencoded({extended: true})); //Allows your app to read from forms.

// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is duplicated if:
	-result from the query not equal to null.
	-req.body.name is equal to  result.name 
*/	

app.post("/tasks", (req, res) => {
	// Call back functions in mongoose methods are programmed this way:
		// first parameter store the error
		// second parameter return the result.
		// req.body.name = eat
		///Task.findOne({name:eat})
	Task.findOne({name:req.body.name}, (err, result) =>{
		console.log(result);

		// If a document was found and the  document's name mataches the information sent by the client/postman.
		if(result != null && req.body.name == result.name){
			// Return a message to the client/postman
			return res.send("Duplicate task found!");
		} 
		// If no document was found or no duplicate.
		else{
			// Create a new task and save to database
			let newTask = new Task({
				name: req.body.name
			});

			// The "save" method will store the infomation to the database
			// Since the "newTask" was created/instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

			newTask.save((saveErr, saveTask) =>{
				// If there are errors in saving it will be displated in the console.
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document it will be save in the database.
				else{
					return res.status(201).send("New task created.")
				}
			})
		}
	})

})
// Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) =>{
	Task.find({}, (err, result) =>{


		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result);
		}
	})
})


//  Activity
app.post("/signup", (req, res) => {

	let newUser = new User({
		username: req.body.username,
		password: req.body.password
	})

	newUser.save((saveErr, saveTask) =>{
		if(saveErr){
			return console.error(saveErr);
		}
		else{
			return res.status(201).send("New user registered.")
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));


